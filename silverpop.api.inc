<?php

require_once dirname(__FILE__) . '/lib/Engage.php';

function silverpop_addrecipient($email, $fname) {

	// Configuration section - replace with your values before running

	// replace with your Engage API endpoint hostname:
	$apiHost = variable_get('silverpop_apihost');;

	// replace with your Engage API username:
	$username = variable_get('silverpop_username');

	// replace with your Engage API password:
	$password = variable_get('silverpop_password');

	$list_id = variable_get('silverpop_database');

	$contact_list_id = variable_get('silverpop_contact_list');

	$visitor_key = '';
	if(isset($_COOKIE['com_silverpop_iMAWebCookie'])) {
		$visitor_key = $_COOKIE['com_silverpop_iMAWebCookie'];

		drupal_set_message("Visitor key Cookie value $visitor_key found.");
	} else {
		drupal_set_message("No visitor key cookie found.");
	}

	try {

	    echo "Logging into Engage API on {$apiHost} as {$username}\n";
	    drupal_set_message("Logging into Engage API on {$apiHost} as {$username}");

	    $engage = new Engage($apiHost);
	    $engage->login($username, $password);


	    echo "Fetching user profile\n";

	    $request = '<LoadUserProfile />';
	    $response = $engage->execute($request);

	    echo "User belongs to organization '{$response->RESULT->ORGANIZATION_NAME}'\n";

	    echo "Fetching list of shared databases, queries, and contact lists\n";

	    $request = '<GetLists><VISIBILITY>1</VISIBILITY><LIST_TYPE>2</LIST_TYPE></GetLists>';

	    $request = "<AddRecipient> <LIST_ID>$list_id</LIST_ID>" .
		'<CREATED_FROM>1</CREATED_FROM>' .
		"<VISITOR_KEY>$visitor_key</VISITOR_KEY>" .
		"<CONTACT_LISTS> <CONTACT_LIST_ID>$contact_list_id</CONTACT_LIST_ID>" .
		'</CONTACT_LISTS> <COLUMN>' .
		'<NAME>Customer Id</NAME>' .
		'<VALUE>123-45-67890</VALUE> </COLUMN>' .
		'<COLUMN> <NAME>EMAIL</NAME>' .
		"<VALUE>$email</VALUE> </COLUMN>" . 
		'<COLUMN> <NAME>Fname</NAME>' .
		"<VALUE>$fname</VALUE> </COLUMN>" .
		'</AddRecipient>';

		try {
		$response = $engage->execute($request);
		} catch (Exception $e) {
		throw new Exception('Login failed: ' . $e->getMessage());
		}


		    if($response->RESULT->SUCCESS == "TRUE") {
			$recipient = $response->RESULT->RecipientId;

			echo "Added Recipient ID $recipient";
			drupal_set_message("Added Recipient ID $recipient");
		    } else { 
			echo "Failed to add recipient";
			drupal_set_message("Failed to add recipient");
		    }

	} catch (Exception $e) {
	    echo $e->getMessage() . "\n";
	    print_r($engage->getLastRequest());
	    print_r($engage->getLastResponse());
	    print_r($engage->getLastFault());
		
            drupal_set_message("AddRecipient exception " . $e->getMessage());

	}



}
