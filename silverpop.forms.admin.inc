<?php

function silverpop_admin() {

  $form = array();

  $form['addnew'] = array(
        '#markup' => l("Add New", "admin/settings/silverpop/add")
  );
  $form['table'] = array(
        '#markup' => silverpop_overview()
  );

  $form['silverpop_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Silverpop Engage username'),
    '#default_value' => variable_get('silverpop_username', ''),
    '#size' => 60,
    '#maxlength' => 255,
  );

  $form['silverpop_password'] = array(
    '#type' => 'password',
    '#title' => t('Silverpop Engage password'),
    '#attributes' => array('value' => variable_get('silverpop_password')),
    '#default_value' => variable_get('silverpop_password', ''),
    '#size' => 60,
    '#maxlength' => 255,
  );

  $form['silverpop_apihost'] = array(
    '#type' => 'textfield',
    '#title' => t('Silverpop API host'),
    '#default_value' => variable_get('silverpop_apihost', ''),
    '#size' => 60,
    '#maxlength' => 255,
  );

  $form['silverpop_database'] = array(
    '#type' => 'textfield',
    '#title' => t('Silverpop Database ID'),
    '#default_value' => variable_get('silverpop_database', ''),
    '#size' => 60,
    '#maxlength' => 255,
  );

  $form['silverpop_contact_list'] = array(
    '#type' => 'textfield',
    '#title' => t('Silverpop Contact List ID'),
    '#default_value' => variable_get('silverpop_contact_list', ''),
    '#size' => 60,
    '#maxlength' => 255,
  );

  $form['silverpop_script_src'] = array(
    '#type' => 'textfield',
    '#title' => t('Silverpop Script Include Source'),
    '#default_value' => variable_get('silverpop_script_src', ''),
    '#size' => 60,
    '#maxlength' => 255,
  );

  $form['silverpop_tracked_domains'] = array(
    '#type' => 'textarea',
    '#wysiwyg' => false,
    '#title' => t('Tracked Domains'),
    '#default_value' => variable_get('silverpop_tracked_domains', ''),
    '#description' => 'Enter a comma-separated list of domains for Silverpop to track',
  );


  $form = system_settings_form($form);

  return $form;

}

function silverpop_overview() {

  $header = array(
    array('data' => t('Event ID')),
    array('data' => t('CSS Class')),
    array('data' => t(''), 'colspan' => 2),
  );

  $sql = "SELECT * from silverpop_settings ORDER BY id DESC";
  $result = db_query($sql);

  if($result->rowCount() == 0) {

    $rows[] = array($data => 'No results found');

  }

  foreach($result as $row) {

    $rows[] = array(
      array('data' => $row->event_id),
      array('data' => $row->css_class),
      array('data' => l("Edit", "admin/settings/silverpop/" . $row->id . "/edit")),
      array('data' => l("Delete", "admin/settings/silverpop/" . $row->id . "/delete")),
    );

  }

  return theme('table', array('header' => $header, 'rows'=> $rows));

}

function silverpop_settings_form($form, &$form_state, $id) {

  $event_id = "";
  $css_class = "";

  if(isset($id)) {

    $sql = "SELECT * FROM silverpop_settings where id = :id limit 1";
    $result = db_query($sql, array(':id' => $id));

    foreach($result as $row) {

      $form['id'] = array(
        '#type' => 'hidden',
        '#value' => $id,
      );

      $event_id = $row->event_id;
      $css_class = $row->css_class;

    }

  }

  $form['event_id'] = array(
    '#type' => 'textfield', //you can find a list of available types in the form api
    '#title' => 'Event ID',
    '#size' => 60,
    '#maxlength' => 255,
    '#default_value' => $event_id,
    '#required' => TRUE, //make this field required
  );

  $form['css_class'] = array(
    '#type' => 'textfield', //you can find a list of available types in the form api
    '#title' => 'CSS Class',
    '#size' => 60,
    '#maxlength' => 255,
    '#default_value' => $css_class,
    '#required' => TRUE, //make this field required
  );

  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;

}

function silverpop_settings_form_validate($form, &$form_state) {

}

function silverpop_settings_form_submit($form, &$form_state) {

  $event_id = $form_state['values']['event_id'];
  $css_class = $form_state['values']['css_class'];

  if(isset($form_state['values']['id'])) {

    $id = $form_state['values']['id'];

    $result = db_update('silverpop_settings')
      ->fields(array(
         'event_id' => $event_id,
         'css_class' => $css_class
      ))
      ->condition('id', $id)
      ->execute();

    drupal_set_message("Settings updated successfully");

  } else {

    db_insert('silverpop_settings') // Table name no longer needs {}
      ->fields(array(
        'event_id' => $event_id,
        'css_class' => $css_class,
      ))
    ->execute();

  drupal_set_message("New settings saved successfully");

  }

  drupal_goto("admin/settings/silverpop");

}

function silverpop_settings_delete_confirm($form, &$form_state, $id) {

  $form['settings'] = array(
    '#type' => 'value',
    '#value' => $id
  );

  return confirm_form($form,
    t('Are you sure you want to delete this entry?'),
    'admin/settings/silverpop',
    t('This action cannot be undone.'),t('Delete'),t('Cancel')
  );

}

function silverpop_settings_delete_confirm_submit($form, &$form_state) {
  $form_values = $form_state['values'];

  if ($form_state['values']['confirm']) {
    $id = $form_state['values']['settings']['#value'];

    $query = "DELETE  FROM {silverpop_settings} where id=:id limit 1";
    $rs = db_query($query, array(':id' => $id));

    drupal_set_message(t('Entry has been deleted successfully.'));}
    drupal_goto("admin/settings/silverpop");
}
